package cn.parsec.snookerscores;

public class FingerPrint {
	public String name = null;
	public float duration = 0;
	public byte[] fingerPrint;
	
	public FingerPrint(String _name, String _duration, String _fp) {
		
		try {
			duration = Float.parseFloat(_duration);
		} catch (NumberFormatException e) {
		}

		try {
			fingerPrint = Util.parseHexBinary(_fp);
		} catch (NumberFormatException e) {
		}
		
		name = _name;
	}
}

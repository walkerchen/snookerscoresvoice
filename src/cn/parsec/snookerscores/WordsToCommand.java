package cn.parsec.snookerscores;

import java.util.Arrays;
import java.util.List;

import android.util.Log;

import cn.parsec.snookerscores.Command.CMD;
import cn.parsec.snookerscores.Command.CMD_TYPE;

public class WordsToCommand {
	enum CHN_NUM_CHAR {INVALID, DIGIT, LING, SHI, BAI}; 
	
	final static String cancel1 = "取消";
	final static String cancel2 = "取消";
	final static String endframe = "结束";

	final static String ling = "零";
	final static String shi = "十";
	final static String bai = "百";

	final static String n[] = {
			"一",
			"二",
			"三",
			"四",
			"五",
			"六",
			"七",
			"八",
			"九"
	};

	final static List<String> nList = Arrays.asList(n);
	
	final static String tbl[] = {
			"零",
			"一",
			"二",
			"三",
			"四",
			"五",
			"六",
			"七",
			"八",
			"九",
			"十",
			"十一",
			"十二",
			"十三",
			"十四",
			"十五",
			"十六",
			"十七",
			"十八",
			"十九",
			"二十",
			"二十一",
			"二十二",
			"二十三",
			"二十四",
			"二十五",
			"二十六",
			"二十七",
			"二十八",
			"二十九",
			"三十",
			"三十一",
			"三十二",
			"三十三",
			"三十四",
			"三十五",
			"三十六",
			"三十七",
			"三十八",
			"三十九",
			"四十",
			"四十一",
			"四十二",
			"四十三",
			"四十四",
			"四十五",
			"四十六",
			"四十七",
			"四十八",
			"四十九",
			"五十",
			"五十一",
			"五十二",
			"五十三",
			"五十四",
			"五十五",
			"五十六",
			"五十七",
			"五十八",
			"五十九",
			"六十",
			"六十一",
			"六十二",
			"六十三",
			"六十四",
			"六十五",
			"六十六",
			"六十七",
			"六十八",
			"六十九",
			"七十",
			"七十一",
			"七十二",
			"七十三",
			"七十四",
			"七十五",
			"七十六",
			"七十七",
			"七十八",
			"七十九",
			"八十",
			"八十一",
			"八十二",
			"八十三",
			"八十四",
			"八十五",
			"八十六",
			"八十七",
			"八十八",
			"八十九",
			"九十",
			"九十一",
			"九十二",
			"九十三",
			"九十四",
			"九十五",
			"九十六",
			"九十七",
			"九十八",
			"九十九",
			"一百",
			"一百零一",
			"一百零二",
			"一百零三",
			"一百零四",
			"一百零五",
			"一百零六",
			"一百零七",
			"一百零八",
			"一百零九",
			"一百一十",
			"一百一十一",
			"一百一十二",
			"一百一十三",
			"一百一十四",
			"一百一十五",
			"一百一十六",
			"一百一十七",
			"一百一十八",
			"一百一十九",
			"一百二十",
			"一百二十一",
			"一百二十二",
			"一百二十三",
			"一百二十四",
			"一百二十五",
			"一百二十六",
			"一百二十七",
			"一百二十八",
			"一百二十九",
			"一百三十",
			"一百三十一",
			"一百三十二",
			"一百三十三",
			"一百三十四",
			"一百三十五",
			"一百三十六",
			"一百三十七",
			"一百三十八",
			"一百三十九",
			"一百四十",
			"一百四十一",
			"一百四十二",
			"一百四十三",
			"一百四十四",
			"一百四十五",
			"一百四十六",
			"一百四十七",
			"一百四十八",
			"一百四十九",
			"一百五十",
			"一百五十一",
			"一百五十二",
			"一百五十三",
			"一百五十四",
			"一百五十五",
			"一百五十六",
			"一百五十七",
			"一百五十八",
			"一百五十九"
	};
	
	static String[] us = {
			"ZERO",
			"ONE",
			"TWO",
			"THREE",
			"FOUR",
			"FIVE",
			"SIX",
			"SEVEN",
			"EIGHT",
			"NINE"
	};
	
	static List<String> usList = Arrays.asList(us);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String samples[] = 
		{
				"十一",
				"一零",
				"一百三零五六",
				"三",
				"三六",
				"七五十",
				"五十六三",
				"八六十五七",
				"九百十三七",
		};
		
		for(int i=0; i<samples.length; i++)
		{
			Command cmd = WordsToCommand.toCommand(samples[i], MainActivity.LANG_CHINESE);
			System.out.println(samples[i] + "=" + cmd.score);
		}
	}

	public static Command toCommand(String word, int lang)
	{
		Command cmd;
		if(lang==MainActivity.LANG_CHINESE)
			cmd = toCommandChinese(word);
		else 
			cmd = toCommandUS(word);
		if(cmd.type == CMD_TYPE.SCORE) 
		{
			if(cmd.score<=0 || cmd.score>=159) cmd.score = 0;
		}
		
		return cmd;
	}
	
	public static Command toCommandUS(String word)
	{
		Command cmd = new Command();

		if(word==null || word.length()==0) return cmd;
		
		if(word.equals("CANCEL") || word.equals("UNDO"))
		{
			cmd.type = CMD_TYPE.COMMAND;
			cmd.command = CMD.UNDO;
		}
		else if(word.equals("FRAME") || word.equals("NEW FRAME"))
		{
			cmd.type = CMD_TYPE.COMMAND;
			cmd.command = CMD.ENDFRAME;
		}
		else
		{
			cmd.type = CMD_TYPE.SCORE;
			long score = 0;
			try
			{
				score = ConvertWordToNumber.parse(word);
				Log.d("WordsToCommand", "English word:" + word + ", result=" + score);
			}catch(Exception e)
			{
			}
			if(score>0 && score<160)
			{
				cmd.score = (int)score;
			}
		}
		return cmd;
	}

	public static Command toCommandUS_sample(String word)
	{
		Command cmd = new Command();

		if(word==null || word.length()==0) return cmd;
		
		String[] words = word.split(" ");
		if(words==null || words.length==0) return cmd;
		
		cmd.type = CMD_TYPE.SCORE;
		
		for(int i=0; i<words.length; i++)
		{
			int idx = usList.indexOf(words[i]);
			if(idx!=-1) cmd.score += idx*Math.pow(10, (words.length-1-i));
		}
		
		return cmd;
	}
	
	public static Command toCommandChinese(String word)
	{
		Command cmd = new Command();

		if(word==null || word.length()==0) return cmd;
		
		if(word.equals(cancel1) || word.equals(cancel2))
		{
			cmd.type = CMD_TYPE.COMMAND;
			cmd.command = CMD.UNDO;
		}
		else if(word.equals(endframe))
		{
			cmd.type = CMD_TYPE.COMMAND;
			cmd.command = CMD.ENDFRAME;
		}
		else
		{
			CHN_NUM_CHAR lastChar = CHN_NUM_CHAR.INVALID;
			StringBuffer fixWord = new StringBuffer();
			for(int i=word.length()-1; i>=0; i--)
			{
				String current = Character.toString(word.charAt(i));
				CHN_NUM_CHAR currentChar = CHN_NUM_CHAR.INVALID;
				if(nList.indexOf(current)!=-1) currentChar = CHN_NUM_CHAR.DIGIT;
				else if(current.equals(ling))  currentChar = CHN_NUM_CHAR.LING;
				else if(current.equals(shi))  currentChar = CHN_NUM_CHAR.SHI;
				else if(current.equals(bai))  currentChar = CHN_NUM_CHAR.BAI;
				
				switch(lastChar)
				{
				case INVALID:
					if(currentChar!=CHN_NUM_CHAR.INVALID)
					{
						fixWord.append(current);
						lastChar = currentChar;
					}
					break;

				case DIGIT:
					if(currentChar==CHN_NUM_CHAR.SHI || currentChar==CHN_NUM_CHAR.LING)
					{
						fixWord.append(current);
						lastChar = currentChar;
					}
					break;

				case SHI:
					if(currentChar==CHN_NUM_CHAR.DIGIT)
					{
						fixWord.append(current);
						lastChar = currentChar;
					}
					break;

				case BAI:
					if(currentChar==CHN_NUM_CHAR.DIGIT)
					{
						fixWord.append(current);
						lastChar = currentChar;
					}
					break;

				case LING:
					if(currentChar==CHN_NUM_CHAR.BAI)
					{
						fixWord.append(current);
						lastChar = currentChar;
					}
					break;
				}
			}
			
			//System.out.println("word=" + word + ", fixWord=" + fixWord.reverse());
			String w = fixWord.reverse().toString();
			if(w!=null && w.length()>0)
			{
				int score = 0;
				for(int i=0; i<tbl.length; i++)
					if(tbl[i].equals(w))
					{
						score = i;
						break;
					}
				
				if(score>0)
				{
					cmd.type = CMD_TYPE.SCORE;
					cmd.score = score;
				}
			}
			
		}
		
		return cmd;
	}
	
}

package cn.parsec.snookerscores;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

public class SNButon extends ImageButton {

	public SNButon(Context context) {
		super(context);
	}

	public SNButon(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SNButon(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}

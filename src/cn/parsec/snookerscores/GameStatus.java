package cn.parsec.snookerscores;

public class GameStatus {
	public int score1;
	public int score2;
	public int total1;
	public int total2;
	
	public String toString()
	{
		return score1 + ":" + score2 + " (" + total1 + ":" + total2 +")";
	}
}

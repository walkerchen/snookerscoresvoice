package cn.parsec.snookerscores;

public class Command {
	public enum CMD_TYPE {UNKNOWN, COMMAND, SCORE};
	public enum CMD {UNKNOWN, UNDO, ENDFRAME};
	
	public CMD_TYPE type = CMD_TYPE.UNKNOWN;
	public CMD command = CMD.UNKNOWN;
	public int score = 0;
	
	public String toString()
	{
		return "" + type + "," + command + "," + score;
	}
}

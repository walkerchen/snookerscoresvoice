/*
 * Copyright (C) 2012 Jacquet Wong
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * musicg api in Google Code: http://code.google.com/p/musicg/
 * Android Application in Google Play: https://play.google.com/store/apps/details?id=com.whistleapp
 * 
 */

package cn.parsec.snookerscores;

import java.io.File;

import com.musicg.wave.WaveHeader;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;

public class ExtRecorderThread extends Thread {
	
	public final static String WAV_PATH = "SnookerScores";
	private ExtAudioRecorder audioRecord;

	private boolean isRecording = false;
	
	public String filePath = Environment.getExternalStorageDirectory() + "/" + WAV_PATH + "/tmp.wav";
	
	public ExtRecorderThread()
	{
		File f = new File(Environment.getExternalStorageDirectory() + "/" + WAV_PATH);
		if(!f.exists())
			f.mkdirs();
		
		audioRecord = ExtAudioRecorder.getInstanse(false);
		audioRecord.setOutputFile(filePath);
		audioRecord.prepare();
	}
	
	public ExtAudioRecorder getAudioRecord(){
		return audioRecord;
	}

	public String getRecFile()
	{
		return filePath;
	}
	
	public boolean isRecording(){
		return this.isAlive() && isRecording;
	}
	
	public void startRecording(){
		try{
			audioRecord.start();
			isRecording = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void stopRecording(){
		try{
			audioRecord.stop();
			audioRecord.release();

			isRecording = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		startRecording();
	}
}
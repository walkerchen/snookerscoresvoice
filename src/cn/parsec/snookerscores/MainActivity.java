package cn.parsec.snookerscores;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Logger;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import cn.parsec.snookerscores.Command.CMD_TYPE;

import com.musicg.*;
import com.musicg.fingerprint.FingerprintSimilarity;
import com.musicg.fingerprint.FingerprintSimilarityComputer;
import com.musicg.wave.Wave;
import com.musicg.wave.WaveFileManager;
import com.musicg.wave.WaveHeader;

public class MainActivity extends Activity implements OnTouchListener, RecognitionListener {
	static {
		System.loadLibrary("pocketsphinx_jni");
	}

	public final static int LANG_CHINESE = 5;
	public final static int LANG_EN_US = 9;

	final static int PLAYER_A = 0;
	final static int PLAYER_B = 1;

	final static String KEY_KB_COPIED = "kb_copied_sussfully";
	final static String KEY_LANG = "lang";
	final static String KEY_SCORE1 = "score1";
	final static String KEY_SCORE2 = "score2";
	final static String KEY_TOTAL1 = "total1";
	final static String KEY_TOTAL2 = "total2";

	String PS_DATA_PATH = Environment.getExternalStorageDirectory() + "/SnookerScoresVoice";

	public static String tag = null;

	Context ctx = this;

	int partialCount = 0;
	
	SNButon btnVoice1, btnVoice2;
	ImageView china, usa, recordingUndo;
	TextView score1, score2, voiceRecText, total1, total2;

	int currentLangType = LANG_CHINESE;
	int currentPlayer = PLAYER_A;
	LinkedList<GameStatus> actions = new LinkedList<GameStatus>();

	// private ExtRecorderThread recorderThread;

	/**
	 * Recognizer task, which runs in a worker thread.
	 */
	RecognizerTask rec;

	/**
	 * Thread in which the recognizer task runs.
	 */
	Thread rec_thread;

	/**
	 * Are we listening?
	 */
	boolean listening;

	ArrayList<FingerPrint> fps = new ArrayList<FingerPrint>();

	boolean inited = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		init();
		loadGame();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void similarityCheck(View v, String recFile) {
		Log.d(tag, "similarityCheck");

		Wave wave = new Wave(recFile);
		byte[] recFP = wave.getFingerprint();
		FingerprintSimilarity similarity;
		int highestIdx = -1;
		float highest = 0;
		for (int i = 0; i < fps.size(); i++) {
			FingerPrint fp = fps.get(i);
			FingerprintSimilarityComputer fingerprintSimilarityComputer = new FingerprintSimilarityComputer(recFP, fp.fingerPrint);
			similarity = fingerprintSimilarityComputer.getFingerprintsSimilarity();
			if (similarity != null) {
				if (similarity.getSimilarity() > highest) {
					highest = similarity.getSimilarity();
					highestIdx = i;
				}
				// Log.d(tag, "vs " + fp.name + ": similarity=" + similarity.getSimilarity() + ",score=" + similarity.getScore());
			}
		}

		if (highestIdx != -1) {
			FingerPrint fp = fps.get(highestIdx);
			Log.d(tag, "Most similiar:" + fp.name + ", similarity=" + highest);
			if (v == btnVoice1)
				score1.setText(fp.name + "," + highest);
			else
				score2.setText(fp.name + "," + highest);
		}

	}

	public static String getSystemProperty() {
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("os.version=" + System.getProperty("os.version") + "\n");
		strBuf.append("os.name=" + System.getProperty("os.name") + "\n");
		strBuf.append("os.arch=" + System.getProperty("os.arch") + "\n");
		return strBuf.toString();
	}

	private void loadDB() {
		InputStream fIn = null;
		InputStreamReader isr = null;
		BufferedReader input = null;
		try {
			fIn = getResources().getAssets().open("fingerprint.dat");
			isr = new InputStreamReader(fIn);
			input = new BufferedReader(isr);
			String line = "";
			while ((line = input.readLine()) != null) {
				String[] comps = line.split(",");
				if (comps != null && comps.length == 3) {
					FingerPrint fp = new FingerPrint(comps[0], comps[1], comps[2]);
					if (fp != null)
						fps.add(fp);
				}
			}
		} catch (Exception e) {
			e.getMessage();
		} finally {
			try {
				if (isr != null)
					isr.close();
				if (fIn != null)
					fIn.close();
				if (input != null)
					input.close();
			} catch (Exception e2) {
				e2.getMessage();
			}
		}
		return;
	}

	private GameStatus currStatus() {
		GameStatus status = new GameStatus();

		status.score1 = Integer.parseInt(score1.getText().toString());
		status.score2 = Integer.parseInt(score2.getText().toString());
		status.total1 = Integer.parseInt(total1.getText().toString());
		status.total2 = Integer.parseInt(total2.getText().toString());

		return status;
	}

	private void updateStatus(GameStatus status) {
		score1.setText("" + status.score1);
		score2.setText("" + status.score2);
		total1.setText("" + status.total1);
		total2.setText("" + status.total2);
	}

	private void undo() {
		Log.d(tag, "UNDO..., action size:" + actions.size());
		GameStatus status = null;
		try {
			actions.removeLast();
			status = actions.getLast();
		} catch (Exception e) {
		}

		if (status != null) {
			Log.d(tag, "status=" + status);
			updateStatus(status);

			voiceRecText.setText(getResources().getText(R.string.undo_done));
		}
	}

	private void loadGame() {
		GameStatus status = new GameStatus();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		status.score1 = prefs.getInt(KEY_SCORE1, 0);
		status.score2 = prefs.getInt(KEY_SCORE2, 0);
		status.total1 = prefs.getInt(KEY_TOTAL1, 0);
		status.total2 = prefs.getInt(KEY_TOTAL2, 0);
		
		updateStatus(status);
		
		saveStatus();
	}
	
	private void saveGame() {
		//Log.d(tag, "Save game...");
		GameStatus status = currStatus();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		Editor editor = prefs.edit();
		
		editor.putInt(KEY_SCORE1, status.score1);
		editor.putInt(KEY_SCORE2, status.score2);
		editor.putInt(KEY_TOTAL1, status.total1);
		editor.putInt(KEY_TOTAL2, status.total2);
		
		editor.commit();
	}
	
	private void newGame() {
		saveStatus();
		// TODO: Process frame data saving and analysis

		score1.setText("0");
		score2.setText("0");
		total1.setText("0");
		total2.setText("0");

		actions.clear();
		voiceRecText.setText(getResources().getText(R.string.new_game_begin));

		// zero scores
		saveStatus();
	}

	private void newFrame() {
		saveStatus();
		// TODO: Process frame data saving and analysis

		GameStatus status = currStatus();

		if (status.score1 > status.score2)
			total1.setText("" + (status.total1 + 1));
		else if (status.score1 < status.score2)
			total2.setText("" + (status.total2 + 1));
		score1.setText("0");
		score2.setText("0");

		actions.clear();

		voiceRecText.setText(getResources().getText(R.string.new_frame_begin));

		// zero scores
		saveStatus();
	}

	private void saveStatus() {
		GameStatus status = currStatus();
		actions.addLast(status);
	}

	private void init() {

		// Load similarity database
		// loadDB();

		// Log.d(tag, "FingerPrint DB size " + fps.size());

		// Initialize voice command dict and engine

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		currentLangType = prefs.getInt(KEY_LANG, currentLangType);
		setConfiguration(currentLangType, true);

		Log.d(tag, getSystemProperty());

		tag = getPackageName();

		btnVoice1 = (SNButon) findViewById(R.id.logo1);
		btnVoice2 = (SNButon) findViewById(R.id.logo2);
		china = (ImageView) findViewById(R.id.china);
		usa = (ImageView) findViewById(R.id.usa);
		score1 = (TextView) findViewById(R.id.textScore1);
		score2 = (TextView) findViewById(R.id.textScore2);
		voiceRecText = (TextView) findViewById(R.id.voiceRecText);
		total1 = (TextView) findViewById(R.id.totalPlayer1);
		total2 = (TextView) findViewById(R.id.totalPlayer2);
		recordingUndo = (ImageView) findViewById(R.id.recordingUndoImage);
		recordingUndo.setImageDrawable(null);

		OnTouchListener undoListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) 
				{
					undo();
					// Last one was the zero score, don't empty it
					if (actions.size() == 1)
						recordingUndo.setImageDrawable(null);
				}
				return true;
			}
		};

		recordingUndo.setOnTouchListener(undoListener);

		OnTouchListener tl = new OnTouchListener() {
			long lastTouchDown = 0;
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (!inited)
				{
					Toast.makeText(ctx, "请等待语音引擎初始化完成...", Toast.LENGTH_SHORT).show();
					return false;
				}

				AlphaAnimation alphaUp;

				int me = event.getAction();

				switch (me) {
				case MotionEvent.ACTION_DOWN:
					// If recording, ignore action
					if(listening) return false;
					
					// Start recording
					Log.d(tag, "Touch: ACTION_DOWN on  " + v);
					lastTouchDown = System.currentTimeMillis();

					startRecording();

					if (v == btnVoice1)
						currentPlayer = PLAYER_A;
					else if (v == btnVoice2)
						currentPlayer = PLAYER_B;

					v.setBackgroundResource(R.drawable.shape_border_sel);
					alphaUp = new AlphaAnimation(0.75f, 1.0f);
			        alphaUp.setFillAfter(true);
			        v.startAnimation(alphaUp);
			        
					recordingUndo.setImageResource(R.drawable.microphone_green);
					voiceRecText.setTextColor(Color.YELLOW);
					voiceRecText.setText(getResources().getString(R.string.begin_listner));

					break;

				case MotionEvent.ACTION_UP:
					// Stop recording
					long upTime = System.currentTimeMillis();
					long diff = upTime - lastTouchDown;
					Log.d(tag, "Touch: ACTION_UP on " + v + ", diff=" + diff);

					stopRecording();

					v.setBackgroundResource(R.drawable.shape_border);
					alphaUp = new AlphaAnimation(1.0f, 0.75f);
			        alphaUp.setFillAfter(true);
					v.startAnimation(alphaUp);

					break;

				case MotionEvent.ACTION_OUTSIDE:
					// Cancel recording
					Log.d(tag, "Touch: ACTION_OUTSIDE on " + v);
					break;
				}

				return false;
			}

		};

		btnVoice1.setOnTouchListener(tl);
		btnVoice2.setOnTouchListener(tl);

		OnTouchListener lang = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				int me = event.getAction();

				if (me == MotionEvent.ACTION_DOWN) {
					if (v == china) {
						Toast.makeText(ctx, "请用汉语数字报分", Toast.LENGTH_SHORT).show();
						currentLangType = LANG_CHINESE;
					} else if (v == usa) {
						Toast.makeText(ctx, "请用英语数字报分", Toast.LENGTH_SHORT).show();
						currentLangType = LANG_EN_US;
					}
					setConfiguration(currentLangType, false);

					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);

					Editor editor = prefs.edit();
					editor.putInt(KEY_LANG, currentLangType);
					editor.commit();
				}

				return true;
			}
		};

		china.setOnTouchListener(lang);
		usa.setOnTouchListener(lang);
	}

	/*
	public WaveHeader getHeaderFromRecorder(){
		WaveHeader waveHeader = recorderThread.getHeader();
		
		AudioRecord audioRecord = recorderThread.getAudioRecord();
		
		int bitsPerSample = 0;
		if (audioRecord.getAudioFormat() == AudioFormat.ENCODING_PCM_16BIT){
			bitsPerSample = 16;
		}
		else if (audioRecord.getAudioFormat() == AudioFormat.ENCODING_PCM_8BIT){
			bitsPerSample = 8;
		}
		
		int channel = 0;
		// whistle detection only supports mono channel
		if (audioRecord.getChannelConfiguration() == AudioFormat.CHANNEL_CONFIGURATION_MONO){
			channel = 1;
		}

		waveHeader = new WaveHeader();
		waveHeader.setChannels(channel);
		waveHeader.setBitsPerSample(bitsPerSample);
		waveHeader.setSampleRate(audioRecord.getSampleRate());
		
		return waveHeader;
	}
	
	private void testSave( byte[] data )
	{
		Wave wave = new Wave(getHeaderFromRecorder(), data);
		
		WaveFileManager waveFileManager=new WaveFileManager(wave);
		waveFileManager.saveWaveAsFile(Environment.getExternalStorageDirectory() + "/snooker1.wav");
	}
	*/

	private void stopRecording() {
		/*
		if (recorderThread != null) {
			recorderThread.stopRecording();
			recorderThread = null;
		}
		*/
		if (this.listening) {
			this.listening = false;
		}
		this.rec.stop();
	}

	private void startRecording() {
		/*
		recorderThread = new ExtRecorderThread();
		recorderThread.start();
		*/
		this.listening = true;
		this.rec.start();
		this.voiceRecText.setText("");
	}

	public void setConfiguration(int type, boolean dispInfo) {
		EngineInitializer initilizer = new EngineInitializer(this, dispInfo);
		Integer[] typeVar = { type, null };
		initilizer.execute(typeVar);
	}

	/** Called when partial results are generated. */
	public void onPartialResults(Bundle b) {
		final MainActivity that = this;
		final String hyp = b.getString("hyp");

		that.voiceRecText.post(new Runnable() {
			public void run() {
//				that.voiceRecText.setText(hyp);
				/*
				Command cmd = WordsToCommand.toCommand(hyp, currentLangType);
				if (cmd.type == CMD_TYPE.SCORE)
				{
					that.voiceRecText.setText(getResources().getString(R.string.are_you) + cmd.score + "?");
				}
				else that.voiceRecText.setText(getResources().getString(R.string.voice_detected));
				*/
				
				if(partialCount>=5 || partialCount<=0) partialCount = 1;
				String info = new String(new char[partialCount]).replace('\0', '.');
				that.voiceRecText.setText(info);
				partialCount++;
			}
		});
	}

	/** Called with full results are generated. */
	public void onResults(Bundle b) {
		final String hyp = b.getString("hyp");
		final MainActivity that = this;
		this.voiceRecText.post(new Runnable() {
			public void run() {
				//that.voiceRecText.setText(hyp);
				parseVoice(hyp);
			}
		});
	}

	private void parseVoice(String hyp)
	{
		Command cmd = WordsToCommand.toCommand(hyp, currentLangType);
		
		Log.d(tag, "Command received: " + cmd);
		
		if (cmd.type == CMD_TYPE.SCORE) {
			voiceRecText.setText("" + cmd.score);
		}
		if(cmd.type != CMD_TYPE.UNKNOWN)
			executeCmd(cmd);
	}
	
	public void executeCmd(Command cmd) {
		if (cmd.type == CMD_TYPE.COMMAND) {
			switch (cmd.command) {
			case UNDO:
				undo();
				break;

			case ENDFRAME:
				newFrame();
				break;
			}
		} 
		else
		{
			if (cmd.type == CMD_TYPE.SCORE && cmd.score > 0) {
				String dispInfo = "";
				
				GameStatus status = currStatus();
				if (currentPlayer == PLAYER_A)
				{
					status.score1 += cmd.score;
					dispInfo += getResources().getText(R.string.player1);
				}
				else
				{
					status.score2 += cmd.score;
					dispInfo += getResources().getText(R.string.player2);
				}
				
				dispInfo += getResources().getText(R.string.added).toString() + cmd.score + getResources().getText(R.string.score).toString();

				updateStatus(status);

				saveStatus();
				
				voiceRecText.setText(dispInfo);
				voiceRecText.setTextColor(getResources().getColor(R.color.BurlyWood));
				
				// Save game data to reload in case of crash
				saveGame();
			}
			else
			{
				voiceRecText.setText(getResources().getString(R.string.result_zero));
			}
		}
		
		// Enable undo
		Log.d(tag, "Action size:" + actions.size());
		if(actions!=null && actions.size()>1) 
			recordingUndo.setImageResource(R.drawable.undo);
		else
			recordingUndo.setImageDrawable(null);
	}

	public void onError(int err) {
		final MainActivity that = this;
		that.voiceRecText.post(new Runnable() {
			public void run() {
				that.voiceRecText.setText(getResources().getString(R.string.no_result));
				
				//parseVoice("NINE");
				
				if(actions!=null && actions.size()>1) 
					recordingUndo.setImageResource(R.drawable.undo);
				else 
					recordingUndo.setImageDrawable(null);
			}
		});
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	protected static String convertWordsToNumbers(String hyp)
	{
		String updatedHyp = "";
		
		String[] wordNum = SegmentNumber.segmentNum(hyp.toLowerCase()).replace(" | ","%").split("%");
				
		for(String word:wordNum){
		try {
			word = word.replace("| ","");
			System.out.println(word);
			if(word.equals(""))
				continue;
			String dig = ConvertWordToNumber.WithSeparator(ConvertWordToNumber.parse(word));
			//System.out.println(dig);
			updatedHyp+=dig+" ";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		System.out.println(updatedHyp);
		return updatedHyp;
	}
	*/

	private static boolean copyAssetFolder(AssetManager assetManager, String fromAssetPath, String toPath) {
		try {
			String[] files = assetManager.list(fromAssetPath);
			new File(toPath).mkdirs();
			boolean res = true;
			for (String file : files) {
				if (file.equals("hmm") || file.equals("lm") || file.equals("tdt_sc_8k") || file.equals("tidigits") || file.equals("hub4wsj_sc_8k"))
					res &= copyAssetFolder(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
				else
					res &= copyAsset(assetManager, fromAssetPath + "/" + file, toPath + "/" + file);
			}
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static boolean copyAsset(AssetManager assetManager, String fromAssetPath, String toPath) {
		InputStream in = null;
		OutputStream out = null;
		try {
			in = assetManager.open(fromAssetPath);
			new File(toPath).createNewFile();
			out = new FileOutputStream(toPath);
			copyFile(in, out);
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}

	class EngineInitializer extends AsyncTask<Integer, Void, Void> {

		public MainActivity ctx;
		String[] defaultConfig = new String[3];
		boolean infoEnabled;

		public EngineInitializer(MainActivity a, boolean dispInfo) {
			ctx = a;
			infoEnabled = dispInfo;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if (ctx.listening) {
				stopRecording();
			}

			ctx.rec = new RecognizerTask(PS_DATA_PATH, defaultConfig[0], defaultConfig[1], defaultConfig[2]);
			ctx.rec_thread = new Thread(ctx.rec);
			ctx.listening = false;
			ctx.rec.setRecognitionListener(ctx);
			ctx.rec_thread.start();
			if (infoEnabled)
				Toast.makeText(ctx, "语音引擎初始化成功！", Toast.LENGTH_SHORT).show();

			if (currentLangType == LANG_CHINESE) {
				china.setBackgroundResource(R.drawable.shape_border_lang);
				usa.setBackgroundColor(Color.TRANSPARENT);
			} else {
				china.setBackgroundColor(Color.TRANSPARENT);
				usa.setBackgroundResource(R.drawable.shape_border_lang);
			}

			inited = true;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (infoEnabled)
				Toast.makeText(ctx, "正在初始化语音引擎，请稍候..", Toast.LENGTH_SHORT).show();
			inited = false;
		}

		@Override
		protected Void doInBackground(Integer... type) {

			switch (type[0]) {

			// default hub4 words
			case 1:
				defaultConfig[0] = "hub4wsj_sc_8k";
				defaultConfig[1] = "hub4.5000.DMP";
				defaultConfig[2] = "hub4.5000.dic";
				break;
			// numbers
			case 2:
				defaultConfig[0] = "hub4wsj_sc_8k";
				defaultConfig[1] = "number.DMP";
				defaultConfig[2] = "number.dic";
				break;
			// digits
			case 3:
				defaultConfig[0] = "tidigits";
				defaultConfig[1] = "tidigits.DMP";
				defaultConfig[2] = "tidigits.dic";
				break;

			// Full chinese
			case 4:
				defaultConfig[0] = "tdt_sc_8k";
				defaultConfig[1] = "gigatdt.5000.DMP";
				defaultConfig[2] = "mandarin_notone.dic";
				break;

			// Chinese numbers
			case 5:
				defaultConfig[0] = "tdt_sc_8k";
				defaultConfig[1] = "snooker-chn.dmp";
				defaultConfig[2] = "snooker-chn.dic";
				break;

			// Chinese numbers, not working
			case 6:
				defaultConfig[0] = "zh_broadcastnews_ptm256_8000";
				defaultConfig[1] = "gigatdt.5000.DMP";
				defaultConfig[2] = "snooker-chn.dic";
				break;

			// Mixture numbers
			case 7:
				defaultConfig[0] = "tdt_sc_8k";
				defaultConfig[1] = "4185.lm";
				defaultConfig[2] = "4185.dic";
				break;

			// Mixture numbers
			case 8:
				defaultConfig[0] = "tdt_sc_8k";
				defaultConfig[1] = "gigatdt.5000.DMP";
				defaultConfig[2] = "snooker.dic";
				break;

			// English full acoustic model with number dict
			case 9:
				defaultConfig[0] = "hub4wsj_sc_8k";
				defaultConfig[1] = "snooker-eng.dmp";
				defaultConfig[2] = "snooker-eng.dic";
				break;

			// Mixture of eng and chs
			case 10:
				defaultConfig[0] = "tdt_sc_8k";
				defaultConfig[1] = "test.lm";
				defaultConfig[2] = "test.dic";
				break;
			}

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);

			File f = new File(PS_DATA_PATH);
			boolean kbCopied = false;
			if (!f.exists()) {
				// Create knowledge base file dir and copy assets to its
				f.mkdirs();
			} else {
				kbCopied = prefs.getBoolean(KEY_KB_COPIED, false);
			}

			if (!kbCopied) {
				// Copy kb from assets to it
				if (copyAssetFolder(ctx.getAssets(), "kb", PS_DATA_PATH)) {
					Editor editor = prefs.edit();
					editor.putBoolean(KEY_KB_COPIED, true);
					editor.commit();
				}
			}
			return null;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.action_new_frame:
			this.newFrame();
			return true;

		case R.id.action_reset:
			this.newGame();
			return true;
			
		case R.id.action_help:
			Intent i = new Intent(this, InfoActivity.class);
			startActivityForResult(i, 1);
			return true;
			
		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}
}
